%{
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

short int valores=1;
int res = 0;
%}


             
/* Declaraciones de BISON */
%union{
	int entero;
	double real;
	char* cadena;
}

%token <entero> ENTERO
%token <cadena> CADENA
%token <cadena> FPOW
%token <real> REAL
%token MAS
%token MENOS
%token POR
%token ENTRE
%token POT
%token MOD
%token PA
%token PC
%token COMA
%token PTCOMA


%type <entero> exp_ent
%type <real> exp_real_mix 
%type <real> exp_real 
%type <cadena> exp_cad   

		
%left MAS MENOS MOD
%left MULT DIV
%left POW FPOW
             
/* Gramática */
%%
             
input:    /* cadena vacía */
        | input line             
;

line:     '\n'
        | operacion '\n' 
        | texto	'\n'
;

operacion: exp_ent {printf("Resultado: %d\n",$1);}
	| exp_real {printf("Resultado: %f\n",$1);}
	| exp_real_mix {printf("Resultado: %f\n",$1);}
;

texto: exp_cad {printf("Cadena: %s\n",$1);} 
;
             

exp_ent:     ENTERO				 { $$ = $1; }
	| MENOS ENTERO				 { $$ = ($2)*(-1);}
	| exp_ent MAS exp_ent	     { $$ = $1 + $3;}
	| exp_ent MENOS exp_ent	     { $$ = $1 - $3;}
	| exp_ent POR exp_ent     	 { $$ = $1 * $3;}
	| FPOW PA exp_ent COMA exp_ent PC PTCOMA { $$ = pow($3,$5);}
	| exp_ent ENTRE exp_ent      {
			if($3 == 0.0){			
				yyerror("Division por cero");
				exit(0);
			}				
			else
				$$ = $1 / $3;
		}	
;
exp_real_mix:     REAL	{ $$ = $1; }
	| exp_real MAS exp_ent        { $$ = $1 + $3;}
	| exp_ent MAS exp_real     { $$ = $1 + $3;}
	| exp_ent MENOS exp_real      { $$ = $1 - $3;}
	| exp_real MENOS exp_real      { $$ = $1 - $3;}
	| exp_ent ENTRE exp_real      {
			if($3 == 0.0){			
				yyerror("Division por cero");
				exit(0);
			}				
			else
				$$ = $1 / $3;
		}
	| exp_real ENTRE exp_ent      {
			if($3 == 0.0){			
				yyerror("Division por cero");
				exit(0);
			}				
			else
				$$ = $1 / $3;
		}
	| exp_ent POR exp_real      { $$ = $1 * $3;}
	| exp_real POR exp_ent      { $$ = $1 * $3;}
	| exp_ent POT exp_real{ $$ = pow($1,$3);}
	| exp_real POT exp_ent { $$ = pow($1,$3);}
	| exp_ent MOD exp_real { $$ = fmod($1,$3);}
	| exp_real MOD exp_ent { $$ = fmod($1,$3);}
;

exp_real: REAL {$$ = $1;}
	| exp_real MAS exp_real { $$ = $1 + $3;}
	| exp_real MENOS exp_real { $$ = $1 - $3;}
	| exp_real ENTRE exp_real {
			if($3 == 0.0){			
				yyerror("Division por cero");
				exit(0);
			}				
			else
				$$ = $1 / $3;
		}
	| exp_real POR exp_real { $$ = $1 * $3;}
	| exp_real POT exp_real { $$ = pow($1,$3);}
	| exp_real MOD exp_real { $$ = fmod($1,$3);}
;

exp_cad: CADENA {$$ = $1; }
	
;





             
%%

int main() {
  yyparse();
}
             
yyerror (char *s)
{
  printf ("--%s--\n", s);
}
            
int yywrap()  
{  
  return 1;  
}  
